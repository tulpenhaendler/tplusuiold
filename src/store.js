import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

/* eslint-disable */

const axios = require('axios');

export default new Vuex.Store({
  state: {
      interval: {

      },
      RuntimeData: {
          DaemonRunning: false,
          Status: '',
          UiSettings: {
              ActiveProject: ''
          }
      },
      Storage: {
          LastImagesUpdate: 0,
          Environments: []
      }
  },
  mutations: {
      setState(state, payload) {
          state.RuntimeData = payload.RuntimeData
          state.Storage = payload.Storage
      },
      setInterval(state, payload) {
        if( typeof state.interval[payload.name] != 'undefined' ) {
            clearInterval(state.interval[payload.name])
        }
        state.interval[payload.name] = payload.interval
      }
  },
  actions: {
    setState(context, payload) {
      context.commit("setState",payload)
    },
      setInterval(context, payload) {
          context.commit("setInterval",payload)
      },
      clearIntervals(c,p){
          Object.keys(c.state.interval).forEach(function (item) {
              clearInterval(c.state.interval[item])
              c.state.interval = {}
          });
      },
      getNode(context, payload) {
        axios.get("http://localhost:10732/node/" + payload.envName,
            { headers: { "TOKEN": context.state.Storage.ApiToken } }
            ).then((response) => {
              payload.sf(response.data)
          })
      },
      getIPFSStats(context, payload) {
          axios.get("http://localhost:10732/ipfs/stats",
              { headers: { "TOKEN": context.state.Storage.ApiToken } }
          ).then((response) => {
              payload.sf(response.data)
          })
      },
      setNodeState(context,payload){
          axios.get("http://localhost:10732/node/"+payload.envName+"/action/" + payload.action,
              { headers: { "TOKEN": context.state.Storage.ApiToken } }
          )
      },
      GetRPC(context, payload){
          axios.post("http://localhost:10732/node/" + payload.envName +"/rpc",
              {url :payload.url , args: payload.args },
              { headers: { "TOKEN": context.state.Storage.ApiToken } }
          ).then((response) => {
              payload.sf(response.data)
          })
      },
      GetTezLogs(context, payload){
          axios.get("http://localhost:10732/node/" + payload.envName +"/logs",
              { headers: { "TOKEN": context.state.Storage.ApiToken } }
          ).then((response) => {
              payload.sf(response.data)
          })
      },
      setNodeResources(context, payload){
          axios.post("http://localhost:10732/node/" + payload.envName +"/resources",
              {ram :payload.ram , cpu: payload.cpu },
              { headers: { "TOKEN": context.state.Storage.ApiToken } }
          ).then((response) => {
          })
      },
      newEnv(context,payload){
          axios.post("http://localhost:10732/env/new",payload,
              { headers: { "TOKEN": context.state.Storage.ApiToken } }
          ).then((response) => {
          })
      }
  }
})
