import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// loaders https://github.com/Hokid/vue-loaders
import 'vue-loaders/dist/vue-loaders.css';
import * as VueLoaders from 'vue-loaders';
import './plugins/element.js'
Vue.use(VueLoaders);

import ToggleButton from 'vue-js-toggle-button'

Vue.use(ToggleButton)

import ProgressBar from 'vuejs-progress-bar'
Vue.use(ProgressBar)


import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
